# build the application
FROM docker.io/enros/ubi-quarkus-native-image:20.2.0-java11 as builder
COPY ./ /project
WORKDIR /project
RUN ./gradlew build -Dquarkus.package.type=native

# run the application
FROM registry.access.redhat.com/ubi8/ubi-minimal:8.1
WORKDIR /work
COPY --from=builder /project/build/*-runner /work/application
# set up permissions for user `1001`
RUN chmod 775 /work /work/application \
  && chown -R 1001 /work \
  && chmod -R "g+rwX" /work \
  && chown -R 1001:root /work
RUN mkdir -p /tmp/qiot/truststore
COPY --chown=1001:root truststore/client.ts /tmp/qiot/truststore/

EXPOSE 8080
USER 1001

CMD ["./application", "-Dquarkus.http.host=0.0.0.0"]
